<?php

use yii\db\Migration;

class m171005_101457_item_index extends Migration
{
    public function safeUp()
    {
        $this->dropIndex('collection', 'item');
        $this->createIndex('collection', 'item', ['collection_id', 'position']);
    }

    public function safeDown()
    {
        $this->dropIndex('collection', 'item');
        $this->createIndex('collection', 'item', 'collection_id');
    }

}
