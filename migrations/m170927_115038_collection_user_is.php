<?php

use yii\db\Migration;
use yii\db\Schema;

class m170927_115038_collection_user_is extends Migration
{
    public function up()
    {
        $this->addColumn('collection', 'user_id', Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn('collection', 'user_id');
    }

}
