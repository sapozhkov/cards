<?php

use yii\db\Migration;

class m171011_064328_offer extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('offer', [
            'id'            => $this->primaryKey(),
            'collection_id' => $this->integer()->notNull(),
            'user1_id'      => $this->integer()->notNull(),
            'user2_id'      => $this->integer()->notNull(),
            'user1_items'   => $this->text()->notNull(),
            'user2_items'   => $this->text()->notNull(),
            'status'        => $this->integer()->notNull(),
            'created_at'    => $this->integer()->notNull(),
            'updated_at'    => $this->integer()->notNull(),
            'expired_at'    => $this->integer()->notNull(),
            'closed_by'     => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('offer_item', [
            'id'            => $this->primaryKey(),
            'user_id'       => $this->integer()->notNull(),
            'offer_id'      => $this->integer()->notNull(),
            'collection_id' => $this->integer()->notNull(),
            'item_id'       => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('offer', 'offer_item', 'offer_id');
        $this->createIndex('user_item', 'offer_item', 'user_id, item_id');

        $this->renameColumn('user_has', 'count', 'count_available');
        $this->addColumn('user_has', 'count_total', $this->integer()->notNull());
        $this->execute('UPDATE user_has SET `count_total`=`count_available`');

    }

    public function safeDown()
    {
        $this->dropTable('offer');
        $this->dropTable('offer_item');
        $this->renameColumn('user_has', 'count_available', 'count');
        $this->dropColumn('user_has', 'count_total');
    }

}
