<?php

namespace app\controllers;

use app\classes\ItemsApi;
use app\classes\ItemStatus;
use app\models\GenerateItems;
use app\models\User;
use app\models\UserHas;
use app\models\UserNeed;
use Yii;
use app\models\Collection;
use app\models\CollectionSearch;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CollectionController implements the CRUD actions for Collection model.
 */
class CollectionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Collection models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CollectionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Collection model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $collection = $this->findModel($id);

        $items = ItemsApi::getByCollectionForUser($collection->id, \Yii::$app->user->id, ItemStatus::NEED);

        return $this->render('view', [
            'model' => $collection,
            'isCollecting' => $collection->isCollectingByUser(\Yii::$app->user->id),
            'items' => $items
        ]);
    }

    /**
     * Creates a new Collection model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Collection([
            'user_id' => \Yii::$app->user->id
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Collection model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ( !$model->belongsToCurrentUser() )
            throw new ForbiddenHttpException("Not allowed to change collection [$id] for user ".\Yii::$app->user->id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Collection model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ( !$model->belongsToCurrentUser() )
            throw new ForbiddenHttpException("Not allowed to change collection [$id] for user ".\Yii::$app->user->id);

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Collection model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Collection the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Collection::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Создает набор элементов
     * @param $id
     * @return string|\yii\web\Response
     * @throws BadRequestHttpException
     * @throws ForbiddenHttpException
     */
    public function actionGenerate($id) {

        try {
            $model = new GenerateItems($id);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ( !$model->collection->belongsToCurrentUser() )
            throw new ForbiddenHttpException("Not allowed to change collection [$id] for user ".\Yii::$app->user->id);

        if ($model->load(Yii::$app->request->post()) && $model->generateItems()) {
            return $this->redirect(['view', 'id' => $model->collection->id]);
        } else {
            return $this->render('generate', [
                'model' => $model,
            ]);
        }


    }

    /**
     * Созданение данных по наличию элементов коллекции у текущего пользователя
     * todo вынести бизнес логику из контроллера
     * @param $collection_id
     */
    public function actionSetAvailability($collection_id) {

        $collection = $this->findModel($collection_id);

        $items = ItemsApi::getByCollectionForUser($collection->id, \Yii::$app->user->id);

        foreach ( $items as $item ) {
            $item_id = $item['id'];
            $newStatus = \Yii::$app->request->post("item-$item_id", null);
            if ( is_null($newStatus) )
                continue;

            $newCount = (int)\Yii::$app->request->post("item-${item_id}-cnt", 0);

            if ( $newStatus != $item['status'] ) {

                // удалить запись старого статуса
                switch ( $item['status'] ) {
                    case ItemStatus::NEED:
                        UserNeed::deleteAll([
                            'user_id' => \Yii::$app->user->id,
                            'item_id' => $item_id
                        ]);
                        break;
                    case ItemStatus::HAS_TO_CHANGE:
                        UserHas::deleteAll([
                            'user_id' => \Yii::$app->user->id,
                            'item_id' => $item_id
                        ]);
                        break;
                    case ItemStatus::HAS:
                    default:
                        // записей в базе нет. удалять нечего
                        break;
                }

                // добавить запись нового статуса
                switch ( (int)$newStatus ) {
                    case ItemStatus::NEED:
                        (new UserNeed([
                            'user_id' => \Yii::$app->user->id,
                            'item_id' => $item_id
                        ]))->save();
                        break;
                    case ItemStatus::HAS_TO_CHANGE:
                        (new UserHas([
                            'user_id' => \Yii::$app->user->id,
                            'item_id' => $item_id,
                            'count_total' => $newCount
                        ]))->save();
                        break;
                    case ItemStatus::HAS:
                    default:
                        // записей в базе нет для статуса. добавлять ничего не надо
                        break;
                }

                // todo можно сделать групповое добавление. это ускорит работу при первичном добавлении

            } else {

                // если статус не менялся - проверим изменения количества для статуса "на обмен"

                if ( $newStatus == ItemStatus::HAS_TO_CHANGE and $newCount != $item['count'] ) {
                    /** @var UserHas $row */
                    $row = UserHas::find()
                        ->where([
                            'user_id' => \Yii::$app->user->id,
                            'item_id' => $item_id,
                        ])
                        ->one()
                    ;
                    if ( $row ) {
                        $row->count_total = $newCount;
                        $row->save();
                    }

                }

            }
        }

        $this->redirect(['view', 'id'=>$collection_id]);

    }

    /**
     * Обмен карточками для текущего пользователя
     * @param int $id идентификатор коллекции
     * @return string
     */
    public function actionFindExchange($id) {

        $model = $this->findModel($id);

        // набор id элементов этой коллекции
        $itemIds = ItemsApi::getIdsByCollection($id);

        /**
         * @var string[] то, что нужно пользователю - набор id элементов
         * (значения numeric, но выбраны строками)
         */
        $userNeedIds = UserNeed::find()
            ->select('item_id')
            ->where([
                'user_id' => \Yii::$app->user->id,
                'item_id' => $itemIds
            ])
            ->asArray()
            ->column()
        ;

        /**
         * другие пользователи с максимальным количеством элементов,
         * доступных "на обмен" из тех, что надо пользователю
         * <pre>
         *   280 => array
         *      'user_id' => '280'
         *      'cards_count' => '153'
         *   542 => ...
         *   ...
         * </pre>
         */
        $others = UserHas::find()
            ->select([
                'user_id',
                'cards_count' => 'COUNT(*)'
            ])
            ->where(['item_id' => $userNeedIds])
            ->groupBy('user_id')
            ->limit(100)
            ->orderBy('COUNT(*) DESC')
            ->asArray()
            ->indexBy('user_id')
            ->all()
        ;

        // набор id элементов, которые есть у пользователя
        $oursId = UserHas::find()
            ->select('item_id')
            ->where([
                'user_id' => \Yii::$app->user->id,
                'item_id' => $itemIds
            ])
            ->asArray()
            ->column()
        ;

        /**
         * Набор пользователей и сколько им надо карточек из нашего наличия "на обмен"
         * <pre>
         *   280 => array
         *      'user_id' => '280'
         *      'cards_count' => '153'
         *   542 => ...
         *   ...
         * </pre>
         */
        $othersNeed = UserNeed::find()
            ->select([
                'user_id',
                'cards_count' => 'COUNT(*)'
            ])
            ->where([
                'user_id' => ArrayHelper::getColumn($others, 'user_id'),
                'item_id' => $oursId
            ])
            ->groupBy('user_id')
            ->orderBy('COUNT(*) DESC')
            ->asArray()
            ->indexBy('user_id')
            ->all()
        ;
        
        /**
         * дополняем массив пользовалей количеством требующихся им карточек,
         * которые есть у нас
         * Добавляем в поле need_count - требуемое количество, которое есть у нас
         * а в to_change - количество, которое можно поменять (пересечение количеств для обмена)
         */
        foreach ($others as $other_id => &$other) {
            if ( isset($othersNeed[$other_id]) )
                $toChangeCount = $othersNeed[$other_id]['cards_count'];
            else
                $toChangeCount = 0;

            $other['need_count'] = $toChangeCount;

            $other['to_change'] = min($toChangeCount, $other['cards_count']);
        }


        /**
         * дополняем массив пользовалей их именами
         * Получаем массив
         * <pre>
         *   280 => array
         *      'user_id' => 280 - id пользователя
         *      'cards_count' => 153 - число элементов у него, которые нужны нам
         *      'need_count' => 10 - число элементов у нас, которые нужны ему
         *      'user_name' => 10 - число элементов у нас, которые нужны ему
         *   542 => ...
         *   ...
         * </pre>
         */
        $userNames = User::find()
            ->select('username,  id')
            ->where(['id' => array_keys($others)])
            ->indexBy('id')
            ->column()
        ;
        foreach ($others as $other_id => &$other) {
            if ( isset($userNames[$other_id]) )
                $other['user_name'] = $userNames[$other_id];
            else
                $other['user_name'] = 'User '.$other_id;
        }

        /**
         * фикция дл сортировки массива пользователей
         * Выше ставятся те, у кого больше пересечение по количеству карточек
         * @param $a
         * @param $b
         * @return int
         */
        $cmp = function ($a, $b) {
            $interceptionA = $a['to_change'];
            $interceptionB = $b['to_change'];
            if ($interceptionA == $interceptionB) {
                return 0;
            }
            return ($interceptionA < $interceptionB) ? 1 : -1;
        };

        uasort($others, $cmp);

        return $this->render('exchange_list', [
            'others' => $others,
            'model' => $model
        ]);

    }

    /**
     * Форма выбора элементов для обмена
     * @param $other_user_id
     * @param $collection_id
     * @return string
     */
    public function actionChangeForm( $other_user_id, $collection_id ) {

        $collection = $this->findModel($collection_id);
        $user = User::findOne($other_user_id);

        $weHaveIds = UserHas::get(\Yii::$app->user->id, $collection_id);
        $weNeedIds = UserNeed::get(\Yii::$app->user->id, $collection_id);

        $theyHaveIds = UserHas::get($other_user_id, $collection_id);
        $theyNeedIds = UserNeed::get($other_user_id, $collection_id);

        // выбрать наши, которые нужны пользователю
        $ourIds = array_intersect($weHaveIds, $theyNeedIds);

        // выбрать чужие, которые нужны нам
        $theirsIds = array_intersect($theyHaveIds, $weNeedIds);

        $minCount = min(count($ourIds), count($theirsIds));

        $items = ItemsApi::getByCollection($collection_id);

        // todo можно пометить сначала те, которых больше одной, потом остальные до нужного количества

        $giveList = [];
        $cnt = $minCount;
        foreach ( $ourIds as $id ) {
            if ( !isset($items[$id]) )
                continue;
            $item = $items[$id];
            $giveList[] = [
                'id' => $id,
                'name' => $item->name,
                'select' => (int)($cnt-- > 0)
            ];
        }

        $getList = [];
        $cnt = $minCount;
        foreach ( $theirsIds as $id ) {
            if ( !isset($items[$id]) )
                continue;
            $item = $items[$id];
            $getList[] = [
                'id' => $id,
                'name' => $item->name,
                'select' => (int)($cnt-- > 0)
            ];
        }

        return $this->render('exchange_form', [
            'collection' => $collection,
            'user' => $user,
            'giveList' => $giveList,
            'getList' => $getList
        ]);

    }

}
