<?php

namespace app\commands;

use app\models\Item;
use app\models\User;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

/**
 * Генератор контента для тестирования
 * Class GeneratorController
 * @package app\commands
 */
class GeneratorController extends Controller {

    /**
     * Очищает базу данных
     */
    public function actionClearDB()
    {
        $aList = [
            'collection',
            'item',
            'user',
            'user_has',
            'user_need',

        ];
        foreach ($aList as $sName)
            $this->query("TRUNCATE `$sName`");
    }

    private function query( $query ) {
        return \Yii::$app->getDb()->createCommand($query)->execute();
    }

    /**
     * Создает первичный набор данных
     * @param int $iMaxCollection
     * @param int $iMinItems
     * @param int $iMaxItems
     * @param int $iMaxUsers
     * @param int $iMaxUserCollections
     */
    public function actionCreate($iMaxCollection=0, $iMinItems=0, $iMaxItems=0, $iMaxUsers=0, $iMaxUserCollections=0)
    {

        if ( !$iMaxCollection)
            $iMaxCollection = $this->prompt('Collections number:', ['required' => true, 'default'=>1000]);

        if ( !$iMinItems )
            $iMinItems = $this->prompt('Min number of items in collection:', ['required' => true, 'default'=>100]);

        if ( !$iMaxItems )
            $iMaxItems = $this->prompt('Max number of items in collection:', ['required' => true, 'default'=>300]);

        if ( !$iMaxUsers )
            $iMaxUsers = $this->prompt('Users number:', ['required' => true, 'default'=>5000]);

        if ( !$iMaxUserCollections )
            $iMaxUserCollections = $this->prompt('Max count of collections for user:', ['required' => true, 'default'=>50]);;


        $this->stdout("Clear base\n");
        $this->actionClearDB();

        $this->stdout("Create $iMaxCollection collections by $iMinItems-$iMaxItems cards in seach");
        $aCollections = [];
        $aCollectionCountList = [];
        for ($i = 1; $i <= $iMaxCollection; $i++) {
            $iItemsNum = rand($iMinItems, $iMaxItems);
            $aCollections[] = "($i,'Набор $i',$iItemsNum)";
            $aCollectionCountList[$i] = $iItemsNum;

            $aItems = [];
            for ($j = 1; $j <= $iItemsNum; $j++)
                $aItems[] = "(NULL,'Карточка $j [в $i наборе]',$i,$j)";
            $this->query("INSERT INTO item(id,name,collection_id,position) VALUES ".implode(',', $aItems));
            $this->stdout('.');
        }
        $this->query("INSERT INTO `collection`(id,name,count) VALUES ".implode(',', $aCollections));
        $this->stdout("\n");


        // создать пользователей
        $this->stdout("Create users");
        $aUsers = [];
        for ($i = 1; $i <= $iMaxUsers; $i++) {
            $aUsers[] = "($i,'Пользователь $i','email$i@example.com')";

            if ( !($i % 100) )
                $this->stdout("\n$i");

            // выбрать ограниченный набор коллекций
            $iCollections = rand(1, $iMaxUserCollections);
            $aUserCollections = [];
            for ($j = 1; $j <= $iCollections; $j++) {
                $iCollectionNum = rand(1, $iMaxCollection);
                $aUserCollections[$iCollectionNum] = $aCollectionCountList[$iCollectionNum];
            }

            // выбрать рандомное количество карточек из каждой
            $aHasRows = [];
            $aNeedRows = [];
            foreach ($aUserCollections as $iCollection => $iUserCollectionCnt) {

                // заполнить часть как имеющиеся, а часть как требующиеся
                $aItems = ArrayHelper::getColumn(Item::find()
                    ->select('id')
                    ->where(['collection_id' => $iCollection])
                    ->asArray()
                    ->all(), 'id')
                ;

                $iHasCnt = rand(1, $iUserCollectionCnt);
                $aHasKeys = (array)array_rand($aItems, $iHasCnt);
                $aHas = [];
                foreach ($aHasKeys as $iKey)
                    $aHas[] = $aItems[$iKey];
                $aNeed = array_diff($aItems, $aHas);

                foreach ($aHas as $sId)
                    $aHasRows[] = "(NULL,$i,$sId,1,1)";

                foreach ($aNeed as $sId)
                    $aNeedRows[] = "(NULL,$i,$sId)";

            }

            if ( $aHasRows )
                $this->query("INSERT INTO user_has(id,user_id,item_id,count_available,count_total) VALUES ".implode(',', $aHasRows));

            if ( $aNeedRows )
                $this->query("INSERT INTO user_need(id,user_id,item_id) VALUES ".implode(',', $aNeedRows));

            $this->stdout('.');
        }
        $this->query("INSERT INTO `user`(id,username,email) VALUES ".implode(',', $aUsers));
        $this->stdout("\n");


        $this->stdout("Done\n\n");


    }

    /**
     * Создает администратора
     * @param $name
     * @param $email
     * @param $pass
     */
    public function actionAddAdmin($name, $email, $pass, $sAccessKey='') {
        $model = User::find()->where(['username' => 'admin'])->one();
        if (empty($model)) {
            $user = new User();
            $user->username = $name;
            $user->email = $email;
            $user->setPassword($pass);
            if ( $sAccessKey )
                $user->auth_key = $sAccessKey;
            else
                $user->generateAuthKey();
            if ($user->save())
                $this->stdout("Создан пользователь '$name'\n\n");

        }
    }

}