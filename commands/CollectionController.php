<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 29.09.2017
 * Time: 18:31
 */

namespace app\commands;
use yii\console\Controller;


class CollectionController extends Controller {

    public function actionFindCollision() {

        //ActiveQuery::
        $response = \Yii::$app->db->createCommand('
            SELECT * FROM `user_need` AS `n`
            LEFT JOIN `user_has` AS `h` ON 1
            WHERE `n`.`user_id` = `h`.`user_id` AND `n`.`item_id` = `h`.`item_id`
            LIMIT 30        
        ')->queryAll();

        if ( $response ) {
            foreach ($response as $row) {
                $this->stderr(sprintf("Collision need/have for user_id=%d, item_id=%d\n", $row['user_id'], $row['item_id']));
            }
        } else {
            $this->stdout("No collisions\n");
        }

    }

}