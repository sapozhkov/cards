<?php

/* @var $this yii\web\View */
/* @var $collection \app\models\Collection */
/* @var $user \app\models\User */
/**
 * @var array  $giveList список позиций для передачи
 * Формат:
 * 0 =>
 *   array (size=3)
 *   'id' => int 200496
 *   'name' => string 'Карточка 48 [в 999 наборе]' (length=41)
 *   'select' => int 1
 * 1 => ...
 */
/** @var array  $getList список позиций для получения. формат как у $giveList */

$this->title = 'Обмен элементами коллекции "' . $collection->name . '" с пользователем "'.$user->username.'"';
$this->params['breadcrumbs'][] = ['label' => 'Collections', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $collection->name, 'url' => ['view', 'id' => $collection->id]];
// todo "Список пользователей для обмена" чтобы можно было назад вернуться
$this->params['breadcrumbs'][] = 'Обмен с пользователем '.$user->username;
?>
<div class="collection-update">

    Имя пользователя: <?= $user->username?>

    <div class="col-sm-6 col-xs-12">
        <h2>Для обмена (наши):</h2>

        <? foreach ($giveList as $item): ?>
            <input type="checkbox" name="our-<?= $item['id'] ?>" id="our-<?= $item['id'] ?>"<? if ($item['select']): ?> checked="checked"<? endif; ?>>
            <label for="our-<?= $item['id'] ?>"><?= $item['name'] ?></label>
            <br>
        <? endforeach; ?>

    </div>

    <div class="col-sm-6 col-xs-12">
        <h2>Для обмена (пользователя):</h2>
        <? foreach ($getList as $item): ?>
            <input type="checkbox" name="their-<?= $item['id'] ?>" id="their-<?= $item['id'] ?>"<? if ($item['select']): ?> checked="checked"<? endif; ?>>
            <label for="their-<?= $item['id'] ?>"><?= $item['name'] ?></label>
            <br>
        <? endforeach; ?>
    </div>



</div>
