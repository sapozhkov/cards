<?php

use app\classes\ItemStatus;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Collection */
/** @var \app\models\Item[] $items */
/** @var bool $isCollecting */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Collections', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="collection-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <? if ( $model->belongsToCurrentUser() ): ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Generate items', ['generate', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <? endif; ?>
    <? if ($isCollecting): ?>
        <p>
            <?= Html::a('Найти с кем поменяться', ['find-exchange', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        </p>
    <? endif; ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'count',
        ],
    ]) ?>

    <? if ($items): ?>
        <h2>Collection items:</h2>

        <? $form = ActiveForm::begin(['action' => ['collection/set-availability', 'collection_id' => $model->id]]); ?>

        <table>
            <tr>
                <th>Название</th>
                <th>Нет</th>
                <th>Есть</th>
                <th>Есть на обмен</th>
            </tr>
            <? foreach ($items as $item): ?>
                <?
                    $count = $item['count'] ? $item['count'] : '';
                    $checkedNeed = '';
                    $checkedHas = '';
                    $checkedHasToChange = '';
                    if ( $item['status'] == ItemStatus::NEED)
                        $checkedNeed = ' checked="checked" ';
                    elseif ( $item['status'] == ItemStatus::HAS)
                        $checkedHas = ' checked="checked" ';
                    else
                        $checkedHasToChange = ' checked="checked" ';
                ?>
                <tr>
                    <td><?= $item['name'] ?></td>
                    <td><input type="radio" name="item-<?= $item['id'] ?>" value="<?= ItemStatus::NEED ?>" <?= $checkedNeed ?>></td>
                    <td><input type="radio" name="item-<?= $item['id'] ?>" value="<?= ItemStatus::HAS ?>" <?= $checkedHas ?>></td>
                    <td>
                        <input type="radio" name="item-<?= $item['id'] ?>" value="<?= ItemStatus::HAS_TO_CHANGE ?>" <?= $checkedHasToChange ?>>
                        <input type="number" name="item-<?= $item['id'] ?>-cnt" value="<?= $count ?>">
                    </td>
                </tr>
            <? endforeach; ?>
        </table>

        <div class="form-group">
            <?= Html::submitButton('Change', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    <? endif; ?>

</div>
