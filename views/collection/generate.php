<?php

/* @var $this yii\web\View */
/* @var $model app\models\GenerateItems */

$this->title = 'Generate items: ' . $model->collection->name;
$this->params['breadcrumbs'][] = ['label' => 'Collections', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->collection->name, 'url' => ['view', 'id' => $model->collection->id]];
$this->params['breadcrumbs'][] = 'Generate items';
?>
<div class="collection-update">

    <?= $this->render('_generate_form', [
        'model' => $model,
    ]) ?>

</div>
