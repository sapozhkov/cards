<?php

use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \app\models\Collection */
/* @var $others array[] */

$this->title = 'Список пользователей для обмена по коллекции: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Collections', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Список пользователей для обмена';
?>
<div class="collection-update">

    <? foreach ($others as $row): ?>

        <a href="<?= Url::to(['change-form', 'other_user_id'=> $row['user_id'], 'collection_id' => $model->id]) ?>" class="list-group-item">
            <div class="list-group-item-text">

                <nobr>
                    <span>
                        <span class="glyphicon glyphicon-user"></span>
                        <?= $row['user_name'] ?>
                    </span>
                </nobr>
                <nobr>
                    <span>
                        <span class="glyphicon glyphicon-refresh"></span>
                        <?= $row['to_change'] ?>
                    </span>
                </nobr>
                <br />

                <nobr>
                    <span title="В наличии: <?= $row['cards_count'] ?>">
                        <span class="glyphicon glyphicon-download"></span>
                        <?= $row['cards_count'] ?>
                    </span>
                </nobr>
                <nobr>
                    <span title="Требуется: <?= $row['need_count'] ?>">
                        <span class="glyphicon glyphicon-upload"></span>
                        <?= $row['need_count'] ?>
                    </span>
                </nobr>

            </div>
        </a>

    <? endforeach; ?>

</div>
