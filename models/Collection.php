<?php

namespace app\models;

use app\classes\ItemsApi;

/**
 * This is the model class for table "collection".
 *
 * @property integer $id
 * @property string $name
 * @property integer $count
 * @property integer $user_id
 */
class Collection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'collection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'count', 'user_id'], 'required'],
            [['count'], 'integer', 'min'=>1, 'max'=>300],
            [['user_id'], 'integer'],
            [['name'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'count' => 'Count',
        ];
    }

    /**
     * Отдает true если коллеция принадлежит текущему активному пользователю
     * @return bool
     */
    public function belongsToCurrentUser() {
        return $this->user_id === \Yii::$app->user->id;
    }

    /**
     * Отдает true если коллекция собирается пользователем
     * @param int $user_id
     * @return bool
     */
    public function isCollectingByUser($user_id) {

        $itemIds = ItemsApi::getIdsByCollection($this->id);

        $need = UserNeed::findOne([
            'item_id' => $itemIds,
            'user_id' => $user_id
        ]);
        if ( $need )
            return true;

        $has = UserHas::findOne([
            'item_id' => $itemIds,
            'user_id' => $user_id
        ]);

        return (bool)$has;

    }

}
