<?php

namespace app\models;

use app\classes\ItemsApi;

/**
 * This is the model class for table "user_need".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $item_id
 */
class UserNeed extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_need';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'item_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'item_id' => 'Item ID',
        ];
    }

    /**
     * Отдает набор элементов коллекции, требующихся пользователю
     * @param int $userId id пользователя
     * @param int $collectionId id коллекции
     * @return int[]
     */
    public static function get($userId, $collectionId) {

        $ids = ItemsApi::getIdsByCollection($collectionId);

        $hasIds = self::find()
            ->select('item_id')
            ->where([
                'user_id' => $userId,
                'item_id' => $ids
            ])
            ->asArray()
            ->column()
        ;

        return array_map('intval', $hasIds);

    }

}
