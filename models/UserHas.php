<?php

namespace app\models;

use app\classes\ItemsApi;

/**
 * This is the model class for table "user_has".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $item_id
 * @property integer $count_available
 * @property integer $count_total
 */
class UserHas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_has';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'item_id', 'count_available', 'count_total'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'item_id' => 'Item ID',
            'count_available' => 'Count Available',
            'count_total' => 'Count Total',
        ];
    }

    /**
     * Отдает набор элементов коллекции, имеющихся у пользователя
     * @param int $userId id пользователя
     * @param int $collectionId id коллекции
     * @return int[]
     */
    public static function get($userId, $collectionId) {

        $ids = ItemsApi::getIdsByCollection($collectionId);

        $hasIds = self::find()
            ->select('item_id')
            ->where([
                'user_id' => $userId,
                'item_id' => $ids
            ])
            ->asArray()
            ->column()
        ;

        return array_map('intval', $hasIds);

    }

}
