<?php

namespace app\models;


use yii\base\InvalidParamException;
use yii\base\Model;
use yii\web\ServerErrorHttpException;

/**
 * Class GenerateItems
 * @package app\models
 * @property Collection $collection
 */
class GenerateItems extends Model
{

    /** @var $this Collection */
    private $_collection;

    public $text = '';

    public function __construct($collection_id, $config = [])
    {

        if (empty($collection_id))
            throw new InvalidParamException('Collection id can not be blank.');

        $this->_collection = Collection::find()->where(['id'=>$collection_id])->one();

        if (!$this->_collection)
            throw new InvalidParamException('Collection not found');

        parent::__construct($config);

        if ( !$this->text )
            $this->text = $this->createBaseSetText();


    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
//            [['id', 'text'], 'required'],
            [['text'], 'string'],
        ];
    }

    public function getCollection() {
        return $this->_collection;
    }

    public function generateItems() {

        $items = explode("\n", $this->text);

        $pos = 1;
        foreach ( $items as $val ) {
            $item = new Item();
            $item->collection_id = $this->collection->id;
            $item->name = $val;
            $item->position = $pos++;

            if ( !$item->save() )
                throw new ServerErrorHttpException('fail to add item');

        }

        return true;

    }

    private function createBaseSetText() {

        $items = [];
        for ($i = 1; $i <= $this->collection->count; $i++)
            $items[] = $i;

        return implode("\n", $items);


    }

}