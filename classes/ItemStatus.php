<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 27.10.2017
 * Time: 9:57
 */

namespace app\classes;


class ItemStatus {

    /**
     * Нет в коллекции
     */
    const NEED = -1;
    /**
     * Есть в коллекции и дополнительные на обмен
     */
    const HAS_TO_CHANGE = 1;
    /**
     * Есть в коллекции
     */
    const HAS = 0;
}