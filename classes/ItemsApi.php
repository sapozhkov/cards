<?php

namespace app\classes;

use app\models\Item;
use app\models\UserHas;
use app\models\UserNeed;
use yii\helpers\ArrayHelper;

class ItemsApi {

    /**
     * Отдает данные по элементам коллекции для указанного пользователя
     * В посылке поля:
     * <pre>
     *  * id - id элемента
     *  * name - название элемента
     *  * status - данные о наличии (смотри константы класса \app\models\Item)
     *      -1 - не хватает
     *      0 - есть
     *      1 - есть на обмен
     *  * count - количество элементов на обмен
     * </pre>
     * @param $collection_id
     * @param $user_id
     * @param int $default_status
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getByCollectionForUser($collection_id, $user_id, $default_status = ItemStatus::HAS) {
        $aList = Item::find()
            ->select('id, name')
            ->where(['collection_id' => $collection_id])
            ->orderBy('position')
            ->asArray()
            ->all();

        $aIdList = ArrayHelper::getColumn($aList, 'id');

        $aHas = UserHas::find()
            ->select('item_id,count_total')
            ->where([
                'item_id' => $aIdList,
                'user_id' => $user_id
            ])
            ->asArray()
            ->all();

        $aIdHas = [];
        foreach ($aHas as $has)
            $aIdHas[(int)$has['item_id']] = (int)$has['count_total'];

        $aNeed = UserNeed::find()
            ->select('item_id')
            ->where([
                'item_id' => $aIdList,
                'user_id' => $user_id
            ])
            ->asArray()
            ->all();

        $aIdNeed = [];
        foreach ($aNeed as $need)
            $aIdNeed[(int)$need['item_id']] = 1;

        foreach ($aList as &$item) {
            $id = (int)$item['id'];
            $count = 0;
            if (isset($aIdNeed[$id])) {
                $status = ItemStatus::NEED;
            } elseif (isset($aIdHas[$id])) {
                $status = ItemStatus::HAS_TO_CHANGE;
                $count = $aIdHas[$id];
            } else {
                // сли есть хоть одна запись
                if ($aHas or $aNeed)
                    // то заполнялось и остальные есть
                    $status = ItemStatus::HAS;
                else
                    // иначе коллекция для пользователя не заполнялась и ни одной нет
                    $status = $default_status;
            }
            $item['status'] = $status;
            $item['count'] = $count;
        }

        return $aList;

    }

    /**
     * Отдает набор id позиций коллекции
     * todo настоить кэш для этой функци - вызывается много раз за проход
     * @param $collection_id
     * @return int[]
     */
    public static function getIdsByCollection($collection_id) {
        $list = Item::find()
            ->select('id, name')
            ->where(['collection_id' => $collection_id])
            ->orderBy('position')
            ->asArray()
            ->all();

        return array_map('intval', ArrayHelper::getColumn($list, 'id'));
    }

    /**
     * Отдает набор id позиций коллекции
     * @param $collection_id
     * @return Item[]
     */
    public static function getByCollection($collection_id) {
        return Item::find()
            ->where(['collection_id' => $collection_id])
            ->orderBy('position')
            ->indexBy('id')
            ->all();
    }
}