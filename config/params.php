<?php

return [
    'adminEmail' => 'a@sapozhkov.ru',
    'user.passwordResetTokenExpire' => 3600,
    'supportEmail' => 'a@sapozhkov.ru'
];
